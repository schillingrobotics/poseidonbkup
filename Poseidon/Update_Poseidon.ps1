# Self elevate to make sure we can kill the process before we start
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Start-Process PowerShell -Verb RunAs "-NoProfile -ExecutionPolicy Bypass -Command `"cd '$pwd'; & '$PSCommandPath';`"";
    exit;
}

#Stop Poseidon if it's currently running
Stop-Process -Name "Poseidon" -Force

#Remove everything from our current folder (Hard coded right now)
Remove-Item "C:\Users\SQAUser\Desktop\Poseidon\*.*" -Exclude "*Update_Poseidon.ps1","*settings.xml"

#Find our newest poseidon version
$newestBuild = gci "\\172.16.0.100\Build_Server\Branches\BB_Poseidon\*" | ? { $_.PSIsContainer } | sort CreationTime -desc | select -f 1

#Copy our poseidon over
Copy-Item -Path "$newestBuild\Poseidon\*" -Destination ".\\" -v -Exclude "*settings.xml"

#Start it up again
Start-Process ".\Poseidon.exe"