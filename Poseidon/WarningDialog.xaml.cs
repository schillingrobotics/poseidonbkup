﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Poseidon
{
    /// <summary>
    /// Interaction logic for WarningDialog.xaml
    /// </summary>
    public partial class WarningDialog : Window
    {
        public bool Result = true;

        private DispatcherTimer _timer = new DispatcherTimer();
        private int _count;

        public WarningDialog(Window sender, int count)
        {
            InitializeComponent();

            Owner = sender;
            Activate();
            Focus();
            cancelButton.Focus();
            _count = count;

            infoTextBlock.Text = "Poseidon will start a scheduled install in " + _count + " seconds.\n\nContinue with install?";

            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += (obj, args) =>
            {
                _count--;
                infoTextBlock.Text = "Poseidon will start a scheduled install in " + _count + " seconds.\n\nContinue with install?";

                if (_count == 0) Close();
                else _timer.Start();
            };

            _timer.Start();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Result = false;
            Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _timer.Stop();
        }
    }
}
