﻿##              Parse Args
## ====================================

# Default values for arguments
$SourceDir = "C:\Temp\Poseidon"
$ConfigType = "copy"
$ConfigDir = "existing"
$SimToolDir = "C:\Users\SQAUser\Desktop"
$OverrideObsolescence = "true"

# Keep a list of devices to Install
$devices = @()

# Parse arguments first to set variables correctly
$SourceDir = $args[0]
$ConfigType = $args[1]
$ConfigDir = $args[2]
$SimToolDir = $args[$args.Count - 2]
$OverrideObsolescence = $args[$args.Count - 1]

for ($i=3; $i -lt $args.Count - 2; $i++) {
	$devices += $args[$i]
}

##              Paths
## ===============================

#$SourceDir = "C:\Users\SQAUser\Desktop\Downloads"
$HHSourceDir = ($SourceDir + "\Installation")
$RTCSourceDir = "$SourceDir\Installation\RTC"
$RTCInstallFile = "INSTALL_RTC.bat"
$RTCUpdateStartupFileDir = "C:\Users\SQAUser\Desktop\Update RTC Startup"
$RTCUpdateStartupFile = "UPDATE_RTC_startup.bat"
$SimToolsSourceDir = $SourceDir
$SimToolsInstallDir = $SimToolDir
$ConfigLocation = ""



##            Parameters
## ================================

$simIPAddress = "192.168.0.147"
$RTCIPAddress = "192.168.0.103"
#$forceHHInstall = If ($args.Length -gt 0) {$args[0]} Else {0}



##               Misc
## ================================

# Map tool names to process names
$simTools = @{}
$simTools.Add("UI_Automation", "UI_Automation_Tester")
$simTools.Add("Logger", "HHLoggingViewer")
$simTools.Add("Amumu", "Amumu Sim")
$simTools.Add("SadRobot", "SadRobotShell")
$simTools.Add("SeaDataLogger", "SeaDataLogger")
$simTools.Add("SeaDataViewer", "SeaDataViewer")
$simTools.Add("AuxGem", "AuxGem")





## TG - 11/19/2018
## ---------------
## To Do:
##   - Incorporate InstallList.ps1 into client install sequence
##   - Check RTC version against install target to determine if it needs to get loaded
##   - Loop through the local souce directory and determine which tools to install
##   - Add command-line option to SadRobot to specify a configuration on launch






## Parse command-line arguments passed in from GUI
## Order
##
##  1) Location of the source files to install
##  2) Options for arg #2: "copy" or "generate"
##  3) Options for arg #3: "old" (can only be specified with "copy" in arg #2) or a path.
##     If arg #2 is "copy", the path is to a folder containing the config files
##     If arg #2 is "generate", the path is to an XML parameter file to generate a new config
##     Examples - "copy old": Copy the config from the previous install
##              - "copy <path>": Copy the config files from the specified folder
##              - "generate <path>": Generate a new config using Logger and the parameters in the specified XML file
##  4) Options for args #4+: names of devices to install to (e.g. - RTC, LeftClient, VideoPC1, etc...)
##
##  WILL ADD MORE AS GUI FEATURES ARE TURNED ON










## Install HH on a client
## Parameter $client can only be the name of a client or video pc
function InstallClient($client) {

    echo "Beginning installation on $client..."

    if ($client -like "*VideoPC*") 
	{
		echo "Client is VideoPC, determining installation directory"

		if (Test-Path "\\$client\SchillingRobotics-VPC\SelectedVersion.xml")
		{
			$SRDir = "SchillingRobotics-VPC"
		}
		elseif (Test-Path "\\$client\SchillingRobotics\SelectedVersion.xml")
		{
			$SRDir = "SchillingRobotics"
		}
		else
		{
			echo "ERROR checking install directory. Installation failed..."
			return
		}
	}
    else 
	{ 
		$SRDir = "SchillingRobotics"
	}
	echo "Installing into directory $SRDir on client $client..."


    # Ensure version to be installed isn't already on client
    echo "  Checking if target version already exists on $client..."

    # Get install target version
    $installTargetVersion = ([xml](Get-Content "$HHSourceDir\VersionInfo.xml")).InstallVersionInfo.VersionNumber

    $versionExists = 0

    # Loop through versions on client and compare to install target version
	for ($i=0; $i -lt 10; $i++) {
	    if (Test-Path "\\$client\$SRDir") { break }
		Start-Sleep 3
	}

	if ($i -gt 9) {
		echo "ERROR: Could not connect to \\$client\$SRDir"
		return
	}

    Get-ChildItem "\\$client\$SRDir" | % {
        # If match, skip copy step
        if ($_.Name.Equals($installTargetVersion)) {
            echo "  Target version ($installTargetVersion) already exists on $client"
            $versionExists = 1
            return
        }
    }

    # Determine current installed version
    $selectedVersionFile = "\\$client\$SRDir\SelectedVersion.xml"  #locate SelectedVersion.xml file
    $selectedVersionXML = [xml](Get-Content $selectedVersionFile)  #parse SelectedVersion.xml file

    # Copy files
    if (!$versionExists -or $forceHHInstall) {
        if (!$versionExists) {echo "  Target version ($installTargetVersion) does not exist on $client. Copying files."}
        else {echo "  Force HH installation specified. Copying files."}

        # Create new folder with name as build version
        mkdir "\\$client\$SRDir\$installTargetVersion" > $null

        # Copy installation files to the new folder
        robocopy "$HHSourceDir" "\\$client\$SRDir\$installTargetVersion" /MIR /NFL /NDL /NJH /NJS

        # Clear read-only flag on all copied files
        Get-ChildItem -Path "\\$client\$SRDir\$installTargetVersion" -Recurse | Where-Object {-not $_.PSIsContainer} | Set-ItemProperty -Name IsReadOnly -Value $false

        del "\\$client\$SRDir\$installTargetVersion\InstallLint.ps1"

        echo "  DONE"
    }
    else { echo "  Skipping copy files." }

	if ($client -notlike "*PC*") { InstallConfig $versionExists }

	# Set new version in SelectedVersion.xml
    echo "  Setting selected version on $client to $installTargetVersion..."
    $selectedVersionXML.NewDataSet.SelectedVersion.SelectedRoot = "F:\$SRDir\$installTargetVersion"  #once the old config is copied over, set the current install version in SelectedVersion.xml
    $selectedVersionXML.Save($selectedVersionFile)

    echo "  DONE"

    Remove-Variable client
    Remove-Variable SRDir

	echo "DONE"
}

function InstallConfig($exists) {

	# Determine whether to copy the old config or import a new one
	if ($ConfigType -eq "generate") {

		# Create a directory for the configGen output
		$configSource = "$SourceDir\Config"
		if (Test-Path $configSource) {
			Get-ChildItem $configSource -Recurse | Remove-Item -Recurse -Force
		}
		else { mkdir $configSource > $null }

		echo "  Generating config..."

		# Parse configGen parameters
		$configGenParamsXML = [xml](Get-Content $ConfigDir)
		$sysType = $configGenParamsXML.Config.SysType.ToUpper()
		$configGenParams = "-path=$configSource -leftstationname=LeftClient -rightstationname=RightClient"
		$configGenParams += " sysname=" + $sysType + "-00"
		$configGenParams += " " + $configGenParamsXML.Config.SysName
		if ($configGenParamsXML.Config.LeftPodUsesHardware -eq "true") { $configGenParams += " lpod" }
		if ($configGenParamsXML.Config.RightPodUsesHardware -eq "true") { $configGenParams += " rpod" }
		if ($configGenParamsXML.Config.ISOL8License -eq "true") { $configGenParams += " isol8" }
		if ($configGenParamsXML.Config.UseAlternateAux -eq "true") { $configGenParams += " usealternateaux" }
		if ($configGenParamsXML.Config.EnableDataSending -eq "true") { $configGenParams += " enabledatasending" }

		# Generate config
		Start-Process -FilePath "$SimToolsInstallDir\Logger\HHLoggingViewer.exe" -ArgumentList $configGenParams
		
		# Wait for config to generate
		Sleep 10
		
		echo "  DONE"
	}
	else {
		if ($ConfigDir -eq "existing") {

			# Locate and parse the previous install startup params xml file
			$previousVersion = $selectedVersionXML.NewDataSet.SelectedVersion.SelectedRoot | Split-Path -Leaf
			$StartUpParamsFile = "\\$client\$SRDir\$previousVersion\ConfigData\StartUp\HH_Start_Up_Params.xml"  
			$StartUpParamsXML = [xml](Get-Content $StartUpParamsFile)

			echo "  Copying existing config from current install ($previousVersion)..."

			# Set source for config copy
			$configFolder = $StartUpParamsXML.NewDataSet.Start_Up_Params.Config_Folder
			$configSource = "\\$client\$SRDir\$previousVersion\ConfigData\$configFolder"
		}
		else {
			echo "  Copying config from $ConfigDir..."

			# Set source for config copy
			$configSource = $ConfigDir
		}
	}

	# Determine config destination directory from startup params file
	$StartUpParamsFile = "\\$client\$SRDir\$installTargetVersion\ConfigData\StartUp\HH_Start_Up_Params.xml"  
	$StartUpParamsXML = [xml](Get-Content $StartUpParamsFile)

	$configFolder = $StartUpParamsXML.NewDataSet.Start_Up_Params.Config_Folder
	$configDestination = "\\$client\$SRDir\$installTargetVersion\ConfigData\$configFolder"

	# If a config already exists for the version we're switching to, don't copy the config
	if ($ConfigType -ne "generate" -and (Test-Path $configDestination)) { echo "  Config exists on $client. Skipping config copy" }
	else {
		echo "  Copying config into place..."

		# Create a config directory in the current install directory and copy the config files
		mkdir "$configDestination" > $null
		robocopy $configSource $configDestination /MIR /NFL /NDL /NJH /NJS
	}
	# Parse configGen parameters
	$configGenParamsXML = [xml](Get-Content $ConfigDir)
	if ($configGenParamsXML.Config.IsWindowedMode -eq "true") 
	{ 
		echo "Windowed mode enabled"
		$StartUpParamsXML.NewDataSet.Start_Up_Params.FullScreenMode = 'false'
		$StartUpParamsXML.Save($StartUpParamsFile)
	}
	else
	{
		echo "Fullscreen mode enabled"
		$StartUpParamsXML.NewDataSet.Start_Up_Params.FullScreenMode = 'true'
		$StartUpParamsXML.Save($StartUpParamsFile)
	}

	if(!$StartUpParamsXML.NewDataSet.Start_Up_Params.OverrideObsolescence)
	{
		$child = $StartUpParamsXML.CreateElement("OverrideObsolescence")
		$StartUpParamsXML.NewDataSet.Start_Up_Params.AppendChild($child)
	}
	$StartUpParamsXML.NewDataSet.Start_Up_Params.OverrideObsolescence = $OverrideObsolescence
	$StartUpParamsXML.Save($StartUpParamsFile)


	# Set config location variable for SadRobot
	$script:ConfigLocation = "$configDestination"

    echo "  DONE"
}

function RebootClient($client) {

	echo "Restarting $client"

	$myCred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList "admin",(New-Object System.Security.SecureString)
    $output = Restart-Computer -ComputerName "$client" -Credential $myCred -Force 2>&1

	if ($output -ne $null) {
		echo $output
		exit 1
	}
	else {
		echo "DONE"
		Remove-Variable output
	}
}





## Install RTC
function InstallRTC {

    echo "Installing RTC..."

    # Run INSTALL_RTC.bat
    echo "  Running $RTCInstallFile..."
    Push-Location -Path $RTCSourceDir
    cmd.exe /c $RTCInstallFile
    Pop-Location
    echo "  DONE"

    # Reboot RTC
    echo "  Rebooting RTC to install files..."
    rebootRTC 240000
    echo "  DONE"

    # Update RTC for sim support
    echo "  Running $RTCUpdateStartupFile..."
    Push-Location $RTCUpdateStartupFileDir
    cmd.exe /c $RTCUpdateStartupFile
    Pop-Location
    echo "  DONE"

	Sleep 5
	
    # Reboot RTC
    echo "  Rebooting RTC to update for simulator support..."
    rebootRTC 90000
    echo "  DONE"
}

function rebootRTC($time) {

    $telnet = New-Object System.Net.Sockets.TCPClient -ArgumentList $RTCIPAddress, 23
    $writer = new-object System.IO.StreamWriter $telnet.GetStream()
    $writer.WriteLine("reboot")
    $writer.Flush()
    $telnet.Close()
    Start-Sleep -m $time
}





## Install sim tools (HH logger, sad robot, amumu)
function InstallSimTools {
    echo "Installing sim tools..."
    $toolList = @()

    # Loop through the souce directory and determine which tools to install, compared against a pre-defined list
    Get-ChildItem -Path "$SimToolsSourceDir" | % {
        if ($simTools.ContainsKey($_.Name)) { $toolList += $_.Name }
    }

    #Install the tools
    foreach ($tool in $toolList) { InstallTool($tool) }

    echo "DONE"
}

function InstallTool($toolName) {

    echo " Installing $toolName..."

    # Kill the existing version of the tool if it's running
    $restartFlag = KillProcess($simTools[$toolName])

    # If the tool already exists on the sim machine, erase the folder
    if (Test-Path "$SimToolsInstallDir\$toolName") {
        Get-ChildItem $SimToolsInstallDir\$toolName -Recurse | Remove-Item -Recurse -Force
    }

    # Create an empty directory for the tool
    else { mkdir "$SimToolsInstallDir\$toolName" > $null }

    # Copy new tool files into place
    echo "  Copying new files"
    robocopy "$SimToolsSourceDir\$toolName\" "$SimToolsInstallDir\$toolName" /MIR /NFL /NDL /NJH /NJS

    # Launch tool
    if ($restartFlag -eq 1) {
        echo ("  Restarting " + $simTools[$toolName])
        Push-Location "$SimToolsInstallDir\$toolName"
        Start-Process ($simTools[$toolName] + ".exe")
        Pop-Location
    }
}

function KillProcess($processName){

    $proc = Get-Process $processName -ErrorAction SilentlyContinue

    if ($proc) {

        echo "  Stopping $processName"
        # Try gracefully first
        $proc.CloseMainWindow()
        Sleep 5

        # Force kill after 5 seconds
        if (!$proc.HasExited) { $proc | Stop-Process -Force }
    }
    else { return 0 }

    Remove-Variable proc

    return 1
}


# DEBUG
# Print args
#for ($i=0; $i -lt $args.Count; $i++) {
#	echo ("Arg " + $i + ": " + $args[$i])
#}
#
#echo ""
#echo ("SimToolsSourceDir: " + $SimToolsSourceDir)
#echo ("HHSourceDir: " + $HHSourceDir)
#echo ""


# Do this first so that we can generate a config if needed
InstallSimTools

if ($devices -contains "RTC") { InstallRTC }

# Install both clients first
if ($devices -contains "LeftClient") { InstallClient "LeftClient" }
if ($devices -contains "RightClient") { InstallClient "RightClient" }

# If SadRobot is running, restart it with the new config
$wasSadRobotRunning = KillProcess $simTools["SadRobot"]

# Kill the amumu process
$wasAmumuRunning = KillProcess $simTools["Amumu"]

echo "Killing Amumu"
Sleep 3

echo "DONE"

if ($wasSadRobotRunning -eq 1) {

	echo "Restarting Sad Robot with new config"

	# Restart Sad robot process
	Push-Location "$SimToolsInstallDir\SadRobot"
    Start-Process ($simTools["SadRobot"] + ".exe") -ArgumentList "/import","$ConfigLocation","/amumu"
    Pop-Location

	# Wait for initialization
	Sleep 5
	
	echo "DONE"
}

echo "Restarting Amumu"
# Restart Amumu process
Push-Location "$SimToolsInstallDir\Amumu"
Start-Process ($simTools["Amumu"] + ".exe")
Pop-Location

# Wait for initialization
Sleep 6

# Restart both clients
if ($devices -contains "LeftClient") { RebootClient "LeftClient" }
if ($devices -contains "RightClient") { RebootClient "RightClient" }

# Install both video pcs first before restarting
if ($devices -contains "VideoPC1") { InstallClient "VideoPC1" }
if ($devices -contains "VideoPC2") { InstallClient "VideoPC2" }
if ($devices -contains "VideoPC1") { RebootClient "VideoPC1" }
if ($devices -contains "VideoPC2") { RebootClient "VideoPC2" }

exit 0