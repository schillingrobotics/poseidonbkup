﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Application = System.Windows.Application;
using CheckBox = System.Windows.Controls.CheckBox;
using ComboBox = System.Windows.Controls.ComboBox;
using Control = System.Windows.Controls.Control;
using MessageBox = System.Windows.MessageBox;

namespace Poseidon
{
    public static class XMLFunctions
    {
        public static bool SaveClassToFile<T>(T classToSave, string folder, string fileName) where T : class, new()
        {
            try
            {
                if (!Directory.Exists(folder)) { Directory.CreateDirectory(folder); }

                var t = classToSave.GetType();
                XmlSerializer s = new XmlSerializer(t);
                using (TextWriter w = new StreamWriter(folder + "//" + fileName))
                {
                    s.Serialize(w, classToSave);
                    w.Close();
                }

                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        public static T PopulateClassFromFile<T>(string filePath) where T : class, new()
        {
            try
            {
                XmlSerializer s = new XmlSerializer(typeof(T));
                T cl = null;
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    cl = s.Deserialize(fs) as T;
                }

                return cl;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }

    public class Config
    {
        public enum SystemType { Gemini, UHDIII, UHDII, HD }
        public enum SystemName { Gemini, UHD3, UHD2, HD }
        public SystemType SysType = SystemType.Gemini;
        public string SysName = SystemName.Gemini.ToString();
        public bool LeftPodUsesHardware = false;
        public bool RightPodUsesHardware = false;
        public bool UseGameControllerHardware = true;
        public bool ISOL8License = true;
        public bool UseAlternateAux = false;
        public bool EnableDataSending = false;
        public bool IsWindowedMode = false;
    }

    public class Settings
    {
        public List<string> Addresses;

        public string SimDirectory;
        public string BranchDir;
        public string SadRobotDir;
        public string AmumuDir;
        public bool IsRTCAnR50;
        public Settings() { }

        public Settings(bool initialize)
        {
            if (initialize)
            {
                Addresses = new List<string>();

                Addresses.Add("LeftClient 192.168.0.101");
                Addresses.Add("RightClient 192.168.0.102");
                Addresses.Add("RTC 192.168.0.103");
                Addresses.Add("VideoPC1 192.168.0.20");
                Addresses.Add("VideoPC2 192.168.0.21");

                BranchDir = "\\\\172.16.0.200\\Build_Server\\Branches\\";
                SadRobotDir = "\\\\172.16.0.200\\Build_Server\\SadRobot\\";
                AmumuDir = "\\\\172.16.0.200\\Build_Server\\Amumu\\";
                SimDirectory = "C:\\Users\\SQAUser\\Desktop";
                IsRTCAnR50 = false;
            }
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public const string INTEGRATION_4_4_8 = "4.4_Integration_Vx7";
        private const string LOG_FILE_PATH = ".\\Log.txt";
        public Config Configuration = new Config();

        private Settings _settings;
        private BackgroundWorker bw;
        private DispatcherTimer _timer = new DispatcherTimer();
        private DispatcherTimer _pbSwitcher = new DispatcherTimer();

        private Dictionary<string, string> _addresses = new Dictionary<string, string>();
        private Dictionary<CheckBox, string> _simTools = new Dictionary<CheckBox, string>();

        private ConfigGen _configGenWindow;
        private ScheduleConfig _scheduleConfigWindow;

        private string configPath = "";
        private bool OperationCancelled = false;
        private bool CanCancelInstall = true;
        private Process _installScript = new Process();
        private bool _overrideObsolescence = true;

        public MainWindow()
        {
            InitializeComponent();
            SetInitialState();
        }

        private void SetInitialState()
        {
            // Create pop up windows
            _configGenWindow = new ConfigGen();
            _scheduleConfigWindow = new ScheduleConfig();

            CreateLogFile();

            // Parse settings.xml to load constants and paths
            _settings = XMLFunctions.PopulateClassFromFile<Settings>("settings.xml");

            // If settings.xml doesn't exist, create a new default settings object and save it to settings.xml
            if (_settings == null)
            {
                _settings = new Settings(true);
                XMLFunctions.SaveClassToFile(_settings, ".", "settings.xml");
            }

            // Map tool names to UI check boxes
            _simTools.Add(HHLoggerCheckBox, "Logger");
            _simTools.Add(seaDataLoggerCheckBox, "SeaDataLogger");
            _simTools.Add(seaDataViewerCheckBox, "SeaDataViewer");
            _simTools.Add(uiAutomationCheckBox, "UI_Automation");
            _simTools.Add(auxGemCheckBox, "AuxGem");

            // Set version label
            Version v = Assembly.GetExecutingAssembly().GetName().Version;
            versionTextBlock.Text = "v." + v.Major + "." + v.Minor + (v.Build != 0 ? "." + v.Build + "." + v.Revision + "" : "");

            //Set build date
            DateTime buildDate = new DateTime(2000, 1, 1)
            .AddDays(v.Build)
            .AddSeconds(v.Revision * 2);
            releaseDateTextBlock.Text = "Release Date: " + buildDate.Date.ToString("MM/dd/yy");

            // Set config radio buttons
            configRadioButton_existing.IsChecked = true;

            // Populate branches combo box
            // This populates the versions combo box too, due to branchComboBox_SelectionChanged event
            populateBranchesComboBox("BB_develop");

            // Populate Sad Robot combo box
            populateSadRobotComboBox();

            // Populate Amumu combo box
            populateAmumuComboBox();

            // Populate sim tool check boxes
            updateCheckBoxes(_simTools);
            sadRobotCheckBox.IsChecked = sadRobotCheckBox.IsEnabled;
            videoPCCheckBox.IsChecked = true;
            overrideCheckBox.IsChecked = true;

            // Populate download/install check boxes
            downloadCheckBox.IsEnabled = installButton.IsEnabled;
            installCheckBox.IsEnabled = installButton.IsEnabled;

            // Update schedule info in main window once the schedule is loaded
            updateScheduleInfo();

            // Set config path
            configPath = sourceDirTextBox.Text + "\\Config";

            // Set automatic schedule trigger action
            _timer.Tick += (sender, args) =>
            {
                // "Click" the refresh button to get the latest builds
                refresh(Path.GetFileName(_scheduleConfigWindow.Schedule.Branch));

                // Pop up a warning window for 15 seconds to allow a user to cancel the install
                // Defaults to starting the install if no input is received
                WarningDialog wd = new WarningDialog(this, 15);
                wd.ShowDialog();

                // Start the install
                if (wd.Result) start(true);

                // Update the schedule with the next install time
                _scheduleConfigWindow.UpdateNextRun();
                updateScheduleInfo();
            };

            // Set progress bar color switching parameters
            _pbSwitcher.Interval = TimeSpan.FromMilliseconds(1);
            _pbSwitcher.Tick += (sender, args) =>
            {
                // Get current progress bar color
                Color c = (progressBar.Foreground as SolidColorBrush).Color;

                // Case 1: R=255, G<255, B=0
                // G++
                if (c.R == 250 && c.G < 250 && c.B == 0) c.G += 50;

                // Case 2: R>0, G=255, B=0
                // R--
                else if (c.R > 0 && c.G == 250 && c.B == 0) c.R -= 50;

                // Case 3: R=0, G=255, B<255
                // B++
                else if (c.R == 0 && c.G == 250 && c.B < 250) c.B += 50;

                // Case 4: R=0, G>0, B=255
                // G--
                else if (c.R == 0 && c.G > 0 && c.B == 250) c.G -= 50;

                // Case 5: R<255, G=0, B=255
                // R++
                else if (c.R < 250 && c.G == 0 && c.B == 250) c.R += 50;

                // Case 6: R=255, G=0, B>0
                // B--
                else if (c.R == 250 && c.G == 0 && c.B > 0) c.B -= 50;

                // Default case: reset to full red
                else
                {
                    c.R = 250;
                    c.G = 0;
                    c.B = 0;
                }

                // Set new progress bar color
                progressBar.Foreground = new SolidColorBrush(c);
            };

            selectedSimDirectoryText.Text = _settings.SimDirectory;
            IsRTCAnR50CheckBox.IsChecked = _settings.IsRTCAnR50;
        }

        private void CreateLogFile()
        {
            // Delete the file if it's older than 5 days, and otherwise create it.
            if(File.Exists(LOG_FILE_PATH))
            {
                FileInfo fi = new FileInfo(LOG_FILE_PATH);
                if (fi.LastAccessTime < DateTime.Now.AddDays(-5))
                    fi.Delete();
            }
            else
            {
                // Create the file
                using (FileStream fs = File.Create(LOG_FILE_PATH)) { };
            }
        }

        #region Support Functions

        public void updateComboBox<T>(ComboBox comboBox, List<T> items, bool reverse = false, object selectedValue = null)
        {
            // Clear the previous items
            comboBox.Items.Clear();

            // Add items from the item list
            foreach (object item in items)
            {
                if (reverse) comboBox.Items.Insert(0, item);
                else comboBox.Items.Add(item);
            }

            // Select an item
            if (selectedValue != null)
            {
                comboBox.SelectedValue = selectedValue;
            }
            else
            {
                comboBox.SelectedIndex = 0;
            }
        }

        private void updateCheckBoxes(Dictionary<CheckBox, string> checkBoxes)
        {
            // Uncheck all checkboxes
            foreach (CheckBox box in checkBoxes.Keys)
            {
                box.IsChecked = false;
            }

            // Exit if either branch or version selection is null
            if (branchComboBox.SelectedValue == null || versionComboBox.SelectedValue == null) return;
            
            UpdateCheckBoxesBasedOnBranch();

            // Enable each checkbox if the tool exists
            try
            {
                // If an RTC folder exists in the "Installation" directory, enable the RTC checkbox
                if (Directory.GetDirectories(branchComboBox.SelectedValue + "\\" + versionComboBox.SelectedValue + "\\Installation", "RTC").Length > 0)
                {
                    RTCCheckBox.IsEnabled = true;
                    RTCCheckBox.IsChecked = true;
                }
                else
                {
                    RTCCheckBox.IsEnabled = false;
                    RTCCheckBox.IsChecked = false;
                }

                // If the tool exists in the specified build folder, enable the checkbox
                foreach (string toolPath in Directory.GetDirectories(branchComboBox.SelectedValue + "\\" + versionComboBox.SelectedValue))
                {
                    foreach (CheckBox box in checkBoxes.Keys)
                    {
                        if (checkBoxes[box] == Path.GetFileName(toolPath)) { box.IsChecked = true; }
                    }
                }
            }
            catch (IOException) { }
            catch (NullReferenceException) { }

            // Disable all unchecked checkboxes (tool does not exist in the specified build folder)
            foreach (CheckBox box in checkBoxes.Keys)
            {
                box.IsEnabled = box.IsChecked == true;
            }
        }

        private void UpdateCheckBoxesBasedOnBranch()
        {
            try
            { 
                if (branchComboBox != null && branchComboBox.SelectedValue != null && 
                    branchComboBox.SelectedValue.ToString().Split('\\').Last() == INTEGRATION_4_4_8)
                {
                    _configGenWindow.SupportsGemini = false;
                }
                else
                {
                    _configGenWindow.SupportsGemini = true;
                }
            }
            catch (Exception ex)
            {
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Unknown Occurred Updating Checkbox State: " + ex.ToString() + "\r"));
                AppendTextToLogFile("Exception number for the previous error" + ex.HResult + "\r");
            }
        }

        private void recursiveCopy(string sourceFolder, string destinationFolder, string destinationName = "")
        {
            // Create copy of source folder in destination folder
            if (destinationName != "") { destinationFolder += "\\" + destinationName; }
            else { destinationFolder += "\\" + Path.GetFileName(sourceFolder); }
            Directory.CreateDirectory(destinationFolder);

            // Copy files
            foreach (string filePath in Directory.GetFiles(sourceFolder))
            {
                if (bw.CancellationPending) throw new TaskCanceledException();
                File.Copy(filePath, destinationFolder + "\\" + Path.GetFileName(filePath));
            }

            // Recursively copy folders
            foreach (string dirPath in Directory.GetDirectories(sourceFolder))
            {
                if (bw.CancellationPending) throw new TaskCanceledException();
                recursiveCopy(dirPath, destinationFolder);
            }

            //If Amumu came in in a zipped file we need to unzip it. 
            var zippedFiles = Directory.GetFiles(destinationFolder, "*.zip", SearchOption.AllDirectories);
            foreach(var file in zippedFiles)
            {
                if (file.Contains("amumu"))
                {
                    ZipFile.ExtractToDirectory(file, destinationFolder);
                    File.Delete(file);

                    var unzippedFolder = Directory.GetFiles(destinationFolder).First();
                    var unzippedFiles = new DirectoryInfo(unzippedFolder).GetFiles();
                    foreach (var nestedFile in unzippedFiles)
                    {
                        nestedFile.MoveTo(destinationFolder);
                    }

                    File.Delete(unzippedFolder);
                }
            }
        }

        private void downloadAndInstall(DoWorkEventArgs e, bool download, bool install, List<string> packages, string destinationDir, string configOptions, string selectedInstallVersion)
        {
            e.Result = false;
            try
            {
                //Check if we're about to brick the RTC
                if (!_settings.IsRTCAnR50 && WillInstallBrickRTC(selectedInstallVersion))
                {
                    OperationCancelled = true;
                    bw.CancelAsync();
                    return;
                }
                if (download)
                {
                    bw.ReportProgress(1, new KeyValuePair<Color, string>(Colors.Black, "Downloading " + XDocument.Load(packages.Find(s => { return s.Contains("Installation"); }) + "\\VersionInfo.xml").Element("InstallVersionInfo").Element("VersionNumber").Value + "...\r\r"));
                    AppendTextToLogFile("Downloading " + XDocument.Load(packages.Find(s => { return s.Contains("Installation"); }) + "\\VersionInfo.xml").Element("InstallVersionInfo").Element("VersionNumber").Value + "...\r\r");
                    if (!downloadFiles(packages, destinationDir, ref e)) { return; }
                    bw.ReportProgress(100, new KeyValuePair<Color, string>(Colors.Black, "\rDownload Complete\r\r"));
                    AppendTextToLogFile("\rDownload Complete\r\r");
                }

                if (install)
                {
                    bw.ReportProgress(100, new KeyValuePair<Color, string>(Colors.Black, "Installing " + XDocument.Load(destinationDir + "\\Installation\\VersionInfo.xml").Element("InstallVersionInfo").Element("VersionNumber").Value + "...\r\r"));
                    AppendTextToLogFile("Installing " + XDocument.Load(destinationDir + "\\Installation\\VersionInfo.xml").Element("InstallVersionInfo").Element("VersionNumber").Value + "...\r\r");
                    if (!installEverything(destinationDir, configOptions)) { return; }
                    bw.ReportProgress(100, new KeyValuePair<Color, string>(Colors.Black, "\rInstallation Complete\r\r"));
                    AppendTextToLogFile("\rInstallation Complete\r\r");
                }

                e.Result = true;
            }
            catch (DirectoryNotFoundException exc) 
            { 
                bw.ReportProgress(100, new KeyValuePair<Color, string>(Colors.Red, "Error determining install version\r\r" + exc.Message + "\r\r"));
                AppendTextToLogFile("Error determining install version\r\r" + exc.Message +":" + exc.HResult +"\r\r");
            }
        }

        private void AppendTextToLogFile(string text)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(LOG_FILE_PATH))
                {
                    sw.WriteLine(text);
                }
            }
            catch
            {

            }
        }

        private bool downloadFiles(List<string> packages, string destinationDir, ref DoWorkEventArgs eventArgs, bool install = false)
        {
            // Clear install directory
            try
            {
                // Recursively remove read-only attributes
                foreach (string filePath in Directory.GetFiles(destinationDir, "*", SearchOption.AllDirectories))
                {
                    (new FileInfo(filePath)).IsReadOnly = false;
                }

                // Delete old files
                Directory.Delete(destinationDir, true);
            }
            catch (DirectoryNotFoundException) { }
            catch (IOException)
            {
                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Red, "Error, failed to clear download directory: " + destinationDir));
                AppendTextToLogFile("Error, failed to clear download directory: " + destinationDir);
                return false;
            }
            catch (Exception ex)
            {
                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Red, "Error, failed to clear download directory: " + destinationDir + "\n Failed with Exception:" + ex.ToString()));
                AppendTextToLogFile("Error, failed to clear download directory: " + destinationDir + "\n Failed with Exception:" + ex.ToString() + ex.HResult);
                return false;
            }

            // Create a new empty directory
            Directory.CreateDirectory(destinationDir);

            // Copy install files for each package (HH, sim tools, etc...)
            foreach (string package in packages)
            {
                string packageName = Path.GetFileName(package);
                if (package.Contains("SadRobot")) { packageName = "SadRobot"; }
                if (package.Contains("Amumu")) { packageName = "Amumu"; }

                bw.ReportProgress((int)((packages.IndexOf(package) / (float)packages.Count) * 100), new KeyValuePair<Color, string>(Colors.Black, "\t" + Path.GetFileName(packageName) + "... "));

                try 
                { 
                    recursiveCopy(package, destinationDir, packageName);
                }
                catch (IOException e)
                {
                    if(OperationCancelled)
                    {
                        updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Orange, "\r\rOperation cancelled\r\r"));
                        AppendTextToLogFile("\r\rOperation cancelled\r\r");
                    }
                    else
                    {
                        bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Red, "FAILED\r" + e.Message));
                        AppendTextToLogFile("FAILED\r" + e.Message + e.HResult);
                    }
                    return false;
                }
                catch (TaskCanceledException)
                {
                    eventArgs.Cancel = true;
                    return false;
                }

                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "done\r"));
                AppendTextToLogFile("done\r");
            }

            OperationCancelled = false;
            return true;
        }

        private bool installEverything(string sourceDir, string configOptions)
        {
            // Determine which devices are available in the bullpen
            string devices = "";
            foreach (KeyValuePair<string, string> device in _addresses)
            {
                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "Looking for " + device.Key + " at " + device.Value + "... "));
                AppendTextToLogFile("Looking for " + device.Key + " at " + device.Value + "... ");
                if (pingCheck(device.Value))
                {
                    bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "found\r"));
                    AppendTextToLogFile("found\r");
                    devices += device.Key + " ";
                }
                else
                {
                    bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "NOT FOUND\r"));
                    AppendTextToLogFile("NOT FOUND\r");

                    if (device.Key.ToLower().Contains("client") || device.Key.ToLower().Contains("rtc"))
                    {
                        bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Red, ("Core device not found: " + device.Key + "\rAborting installation\r")));
                        AppendTextToLogFile("Core device not found: " + device.Key + "\rAborting installation\r");
                        return false;
                    }
                }
            }

            //Clear the process if it was previously doing something
            _installScript.Dispose();
            // Set up install script process
            _installScript = new Process();
            _installScript.StartInfo.FileName = "powershell.exe";
            _installScript.StartInfo.Arguments = ".\\install_everything.ps1 " + sourceDir + " " + configOptions + " " + devices + " " + _settings.SimDirectory + " " + _overrideObsolescence.ToString().ToLower();
            _installScript.StartInfo.UseShellExecute = false;
            _installScript.StartInfo.CreateNoWindow = true;
            _installScript.StartInfo.RedirectStandardOutput = true;
            _installScript.StartInfo.RedirectStandardError = true;

            // Redirect powershell output to MainWindow output text box
            _installScript.OutputDataReceived += (obj, args) => bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, args.Data + "\r"));
            _installScript.ErrorDataReceived += (obj, args) => bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Red, args.Data + "\r"));

            // Launch install script asynchronously and start redirecting output
            _installScript.Start();
            _installScript.BeginOutputReadLine();
            _installScript.BeginErrorReadLine();

            // Block and wait for install script to finish
            // This won't hold up the UI since we're already on a background thread
            _installScript.WaitForExit();
            CanCancelInstall = true;
            return (_installScript.ExitCode == 0);
        }

        private bool pingCheck(string address)
        {
            Ping p = new Ping();
            return (p.Send(address).Status == IPStatus.Success);
        }

        private void shiftLeft(Control control, double initial, double amount)
        {
            control.Margin = new Thickness(initial - amount,
                control.Margin.Top,
                control.Margin.Right,
                control.Margin.Bottom);
        }

        private void updateOutputTextBox(KeyValuePair<Color, string> text)
        {
            TextRange textToAdd = new TextRange(outputTextBox.Document.ContentEnd, outputTextBox.Document.ContentEnd);
            if(text.Value.Contains("Installing RTC...")) 
            {
                CanCancelInstall = false;
            }
            textToAdd.Text = System.DateTime.Now.ToString("HH:mm:ss.fff: ") + text.Value;
            AppendTextToLogFile(textToAdd.Text);
            textToAdd.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(text.Key));
        }

        private void populateBranchesComboBox(string selectedItem)
        {
            try
            {
                List<KeyValuePair<string, string>> branches = new List<KeyValuePair<string, string>>();
                foreach (string path in Directory.GetDirectories(_settings.BranchDir))
                {
                    branches.Add(new KeyValuePair<string, string>(Path.GetFileName(path), path));
                }

                installButton.IsEnabled = true;

                var previouslySelectedItem = branchComboBox.SelectedItem;
                branchComboBox.DisplayMemberPath = "Key";
                branchComboBox.SelectedValuePath = "Value";

                // Triggers populateVersionsComboBox via branchComboBox_SelectionChanged event
                updateComboBox(branchComboBox, branches, false, _settings.BranchDir + selectedItem);

                //Reset the branch combobox to what it was at
                if(branchComboBox.Items.Contains(previouslySelectedItem))
                {
                    branchComboBox.SelectedItem = previouslySelectedItem;
                }
            }
            catch (IOException e)
            {
                installButton.IsEnabled = false;
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Error loading branches: " + e.Message + "\r"));
                AppendTextToLogFile("Exception number for the previous error" + e.HResult + "\r");
            }

            //Re-enable the download and install buttons
            downloadCheckBox.IsEnabled = installButton.IsEnabled;
            installCheckBox.IsEnabled = installButton.IsEnabled;
        }

        private void populateVersionsComboBox()
        {
            try
            {
                List<KeyValuePair<string, string>> versions = new List<KeyValuePair<string, string>>();
                foreach (string path in Directory.GetDirectories(branchComboBox.SelectedValue as string))
                {
                    // Only add versions that have a VersionInfo.xml file (complete builds)
                    try
                    {
                        // Load HH build version
                        string HHVersion = XDocument.Load(path + "\\Installation\\VersionInfo.xml").Element("InstallVersionInfo").Element("VersionNumber").Value;

                        // Load RTC build number
                        string RTCVersion = "";
                        try
                        {
                            StreamReader rtcBuildFile = File.OpenText(path + "\\Installation\\RTC\\BuildNo.h");

                            string line = rtcBuildFile.ReadLine();
                            while (line != null)
                            {
                                if (line.Contains("RTC_BUILD_NO"))
                                {
                                    RTCVersion = line.Trim().Substring(line.Trim().LastIndexOf(' ')).Trim();
                                    break;
                                }

                                line = rtcBuildFile.ReadLine();
                            }

                            RTCVersion = ", " + RTCVersion;
                        }

                        // Caught from File.OpenText(BuildNo.h). Missing or incomplete RTC build... list it without RTC build number
                        catch (IOException) { }

                        string versionInfo = HHVersion + RTCVersion;

                        versions.Add(new KeyValuePair<string, string>(Path.GetFileName(path) + "    (" + versionInfo + ")", Path.GetFileName(path)));
                    }

                    // Caught from XDocument.Load(VersionInfo.xml). Incomplete build... don't list it
                    catch (IOException) { }
                }

                versionComboBox.DisplayMemberPath = "Key";
                versionComboBox.SelectedValuePath = "Value";

                updateComboBox(versionComboBox, versions, true);

                if (versions.Count < 1) throw new IOException("No acceptable builds found for the current branch");

                installButton.IsEnabled = true;
            }
            catch (Exception e)
            {
                if (e is IOException || e is ArgumentNullException)
                {
                    installButton.IsEnabled = false;
                    updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Error loading versions" + (e is IOException ? (": " + e.Message) : "") + "\r"));
                    AppendTextToLogFile("Exception number for the previous error" + e.HResult + "\r");
                }
            }

            //Re-enable the download and install buttons
            downloadCheckBox.IsEnabled = installButton.IsEnabled;
            installCheckBox.IsEnabled = installButton.IsEnabled;
        }

        private void populateSadRobotComboBox()
        {
            try
            {
                List<KeyValuePair<string, string>> versions = new List<KeyValuePair<string, string>>();
                foreach (string path in Directory.GetDirectories(_settings.SadRobotDir))
                {
                    string f = Path.GetFileName(path);

                    try { f = f.Substring(f.IndexOf("_") + 1); }
                    catch (ArgumentNullException) { }

                    versions.Add(new KeyValuePair<string, string>(f, path));
                }

                sadRobotComboBox.DisplayMemberPath = "Key";
                sadRobotComboBox.SelectedValuePath = "Value";

                updateComboBox(sadRobotComboBox, versions, true);
            }
            catch (IOException e)
            {
                sadRobotCheckBox.IsEnabled = false;
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Error loading Sad Robot builds: " + e.Message + "\r\r"));
                AppendTextToLogFile("Exception number for the previous error" + e.HResult + "\r");
            }
        }

        private void populateAmumuComboBox()
        {
            try
            {
                List<KeyValuePair<string, string>> versions = new List<KeyValuePair<string, string>>();
                foreach (string path in Directory.GetDirectories(_settings.AmumuDir))
                {
                    string f = Path.GetFileName(path);

                    try { f = f.Substring(f.IndexOf("_") + 1); }
                    catch (ArgumentNullException) { }

                    versions.Add(new KeyValuePair<string, string>(f, path));
                }

                amumuComboBox.DisplayMemberPath = "Key";
                amumuComboBox.SelectedValuePath = "Value";

                updateComboBox(amumuComboBox, versions, true);
            }
            catch (IOException e)
            {
                amumuCheckBox.IsEnabled = false;
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Error loading Amumu builds: " + e.Message + "\r\r"));
                AppendTextToLogFile("Exception number for the previous error" + e.HResult + "\r");
            }
        }

        private void updateScheduleInfo()
        {
            // Set schedule info in MainWindow
            Schedule schedule = _scheduleConfigWindow.Schedule;
            branchText.Text = schedule.Branch == "" ? "--" : Path.GetFileName(schedule.Branch);
            nextRunText.Text = schedule.IsEnabled ? (schedule.NextRun.Year == 1 ? "Never" : schedule.NextRun.ToString()) : "Disabled";
            previousRunText.Text = schedule.PreviousRun.Year == 1 ? "N/A" : schedule.PreviousRun.ToString();

            // Stop current schedule timer
            _timer.Stop();

            // Check to see if scheduled installation in enabled and that a the next run time exists
            if (schedule.IsEnabled && schedule.NextRun.Year != 1)
            {
                // Set timer to start at next scheduled run + 10ms to avoid a race condition
                _timer.Interval = (schedule.NextRun - DateTime.Now).Add(TimeSpan.FromMilliseconds(10));
                _timer.Start();
            }
        }

        private void start(bool automated = false)
        {
            // Download/install parameters
            bool download = automated || downloadCheckBox.IsChecked == true;
            bool install = automated || installCheckBox.IsChecked == true;
            string destinationDir = sourceDirTextBox.Text;
            string configOptions = "copy existing";

            string branch = String.Empty;
            string version = String.Empty;

            if (branchComboBox.SelectedValue != null && versionComboBox.SelectedValue != null)
            {
                branch = branchComboBox.SelectedValue.ToString();
                version = versionComboBox.SelectedValue.ToString();
            }
            

            // If no operations are specified, print a message and do nothing
            if (!download && !install)
            {
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Black, "Nothing to do!\r"));
                return;
            }

            // If download/install path is empty, display an error and exit
            if (destinationDir == "")
            {
                //updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Download/Installation Source directory not specified\r"));
                MessageBox.Show("Download/Installation Source directory not specified\nPlease specify a directory and try again",
                                "Error: No source directory specified",
                                MessageBoxButton.OK);
                return;
            }

            // Display a warning if only the "install" check box is checked
            if (!download && install)
            {
                if (MessageBox.Show("Ensure the necessary files are in the specified installation source directory",
                        "Warning: Only Installation Specified",
                        MessageBoxButton.OKCancel, MessageBoxImage.Warning) != MessageBoxResult.OK) return;
            }

            // Check for bad RTC builds
            if (RTCCheckBox.IsChecked == true)
            {
                string fileLocation = download ? branch + "\\" + version : destinationDir;
                fileLocation += "\\Installation\\RTC";

                if (File.Exists(fileLocation + "\\build.err") || !File.Exists(fileLocation + "\\rtcBuild.tar"))
                {
                    MessageBox.Show(
                        "Bad RTC build detected (build.err present or rtcBuild.tar missing).\nSelect another build or uncheck the RTC installation option.\n\nAborting operation",
                        "Error: Bad RTC build", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            // If install is specified and "generate" config option is specified, save the configGen parameters to a file
            // Skip if doing an automated install, since we want to use the default behavior (copy existing config)
            if (install && !automated)
            {
                // Determine where the config comes from
                if (configRadioButton_generate.IsChecked == true)
                {
                    XMLFunctions.SaveClassToFile(Configuration, ".", "configGenParams.xml");
                    configOptions = "generate configGenParams.xml";
                }
                else if (configRadioButton_import.IsChecked == true)
                {
                    // Extract .SRBackUp file to download/install source location
                    try
                    {
                        // Copy config file and change extension to .zip so it can be extracted
                        string configCopy = destinationDir + "\\" + Path.GetFileNameWithoutExtension(configPath) + ".zip";
                        File.Copy(configPath, configCopy);

                        // Extract config data
                        ZipFile.ExtractToDirectory(configCopy, destinationDir + "\\Config");

                        // Delete copied config file
                        File.Delete(configCopy);

                        // Set config option
                        configOptions = "copy " + destinationDir + "\\Config\\ConfigurationData";
                    }
                    catch (IOException e)
                    {
                        MessageBox.Show(e.Message, "Error extracting config", MessageBoxButton.OK);
                        AppendTextToLogFile("Exception number for the previous error" + e.HResult + "\r");
                        return;
                    }
                }
            }

            // Build list of items to download/install
            List<string> packages = new List<string>();
            packages.Add(branch + "\\" + version + "\\Installation");

            // Add sim tools to download/install packages if their check boxes are checked
            // If this is an automated install, default to add all available sim tools
            foreach (string tool in _simTools.Where(x => automated ? x.Key.IsEnabled : x.Key.IsChecked == true).Select(x => x.Value).ToList())
            {
                packages.Add(branch + "\\" + version + "\\" + tool);
            }
            if (automated || sadRobotCheckBox.IsChecked == true) { packages.Add((sadRobotComboBox.SelectedValue as string) + "\\SadRobot"); }

            if (automated || amumuCheckBox.IsChecked == true) { packages.Add(amumuComboBox.SelectedValue as string); }

            // Map device names to addresses
            _addresses.Clear();
            foreach (string addressPair in _settings.Addresses) { _addresses.Add(addressPair.Split(' ')[0], addressPair.Split(' ')[1]); }

            // Adjust devices to install based on device checkboxes
            if (RTCCheckBox.IsChecked == false) _addresses.Remove("RTC");
            if (videoPCCheckBox.IsChecked == false)
            {
                foreach (string key in _addresses.Keys.ToList().FindAll(x => x.ToLower().Contains("videopc"))) { _addresses.Remove(key); }
            }

            var selectedInstallVersion = String.Empty;
            if (versionComboBox.SelectedItem != null)
            {
                selectedInstallVersion = versionComboBox.SelectedItem.ToString().Split('(').Last().Split(',').First();
            }

            _overrideObsolescence = DevOverrideCheckBox.IsChecked.Value;

            // Setup download/install processes
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += (obj, args) => downloadAndInstall(args, download, install, packages, destinationDir, configOptions, selectedInstallVersion);
            bw.ProgressChanged += (obj, args) =>
            {
                // Update output text box and progress bar
                updateOutputTextBox((KeyValuePair<Color, string>) args.UserState);
                if (args.ProgressPercentage != 0) progressBar.Value = args.ProgressPercentage;

                // If we're installing the RTC then we should not cancel the script as it can cause the RTC to become bricked
                if (CanCancelInstall == false) 
                { 
                    installButton.IsEnabled = false; 
                }
            };
            bw.RunWorkerCompleted += (obj, args) =>
            {
                // Remove configGenParams.xml if it exists
                try { File.Delete("configGenParams.xml"); }
                catch (IOException) { }

                // Stop progress bar color switching
                _pbSwitcher.Stop();

                // If the operation was cancelled, update the output
                if (args.Cancelled)
                {
                    updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Orange, "\r\rOperation cancelled\r\r"));
                    progressBar.Value = 0;
                }

                // If we have a result, update the output
                else if (args.Result != null)
                {
                    if ((bool) args.Result)
                    {
                        updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Black, "\r------------------------------\r\rSuccess!\r\r"));
                        progressBar.Foreground = new SolidColorBrush(Colors.LimeGreen);
                    }
                    else
                    {
                        if(OperationCancelled)
                        {
                            updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Orange, "\r\rOperation cancelled\r\r"));
                        }
                        else
                        {
                            updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "\r------------------------------\r\rFAILED\r\r"));
                        }

                        progressBar.Foreground = new SolidColorBrush(Colors.Red);
                    }
                }

                // Otherwise display an error
                else
                {
                    updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Orange, "\r-------------------\rUNKNOWN RESULT\r\r"));
                    progressBar.Foreground = new SolidColorBrush(Colors.Orange);
                }

                // Re-enable the "Go" button
                installButton.Content = "Go!";
                installButton.FontSize = 20;
                installButton.IsEnabled = true;

                // Return window to normal behavior
                Topmost = false;
            };

            // Launch download/install process on background thread
            bw.RunWorkerAsync();

            // Switch install button to "Cancel" button until the download process completes
            installButton.Content = "Stop";
            installButton.FontSize = 16;

            // Change window to always be on top
            Topmost = true;

            // Start progress bar color switching
            _pbSwitcher.Start();
        }

        private void refresh(string selectedItem = "")
        {
            try
            {
                // If no selected item is specified, set selectedItem to the current item in the branch combo box (as long as it's not empty)
                if (selectedItem == "" && branchComboBox.Items.Count != 0 && branchComboBox.SelectedItem != null)
                {
                    selectedItem = ((KeyValuePair<string, string>)branchComboBox.SelectedItem).Key;
                }

                // Refresh all combo boxes
                // Don't need to update the versions combo box since populating the branches combo box triggers branchComboBox_SelectionChanged, which populates the versions combo box
                populateBranchesComboBox(selectedItem);
                populateSadRobotComboBox();
                populateAmumuComboBox();
            }
            catch(Exception ex)
            {
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Unknown Error Occurred loading branches: " + ex.ToString() + "\r"));
            }
        }

        #endregion

        #region Element Actions

        private void installButton_Click(object sender, RoutedEventArgs e)
        {
            if (installButton.Content.ToString() == "Stop")
            {
                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "\n"));
                CancelOperation();
            }
            else
            {
                start();
            }
        }

        private void CancelOperation()
        {
            OperationCancelled = true;
            bw.CancelAsync();
            if (_installScript != null)
            {
                try
                {
                    _installScript.Kill();
                    _installScript.WaitForExit();
                }
                catch (Exception ex)
                {
                }
            }
            CanCancelInstall = true;
        }

        private bool WillInstallBrickRTC(string selectedInstallVersion)
        {
            try
            {
                if (selectedInstallVersion == String.Empty)
                    return false;

                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "Checking that this install will not brick the RTC\r"));
                bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "Looking for Left Client at 192.168.0.101\r"));

                if (pingCheck("192.168.0.101"))
                {
                    bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "Found\r"));
                    var doc = new XmlDocument();
                    doc.Load("\\\\LeftClient\\SchillingRobotics\\SelectedVersion.xml");
                    var installedVersion = String.Empty;
                    if (doc.LastChild.InnerText != null && doc.LastChild.InnerText != string.Empty)
                    {
                        installedVersion = doc.LastChild.InnerText.Split('\\').Last();
                    }

                    bw.ReportProgress(0, new KeyValuePair<Color, string>(Colors.Black, "Installed version: " + installedVersion + "\r"));

                    var installedVersionCmp = new Version(installedVersion);
                    var firstVxWorksSevenBuild = new Version("4.5.2.00000");
                    var versionToBeInstalled = new Version(selectedInstallVersion); //Format of items "VersionName  (x.x.x.xxxxx, yyyy)"
                    if (installedVersionCmp > firstVxWorksSevenBuild && versionToBeInstalled < firstVxWorksSevenBuild)
                    {
                        var cancelOperation = false;

                        //Please don't call me on this.
                        //Sleep here to allow the bw to write our previously written messages before calling the main thread. 
                        Thread.Sleep(200);
                        this.Dispatcher.Invoke((Action)(() =>
                        {

                            var dr = MessageBox.Show("It's possible that downgrading from " + installedVersionCmp + " To " + versionToBeInstalled +
                                " could result in a bricked RTC. Please ensure you're installing the right version, and have the correct hardware.\n\nIf you have an R50 RTC, check the box at the bottom of Poseidon to disable this warning.",
                                "Potentially Dangerous Install",
                                MessageBoxButton.OKCancel,
                                MessageBoxImage.Warning, MessageBoxResult.Cancel, System.Windows.MessageBoxOptions.DefaultDesktopOnly);

                            AppendTextToLogFile("It's possible that downgrading from " + installedVersionCmp + " To " + versionToBeInstalled +
                                " could result in a bricked RTC. Please ensure you're installing the right version, and have the correct hardware.\n\nIf you have an R50 RTC, check the box at the bottom of Poseidon to disable this warning.");

                            if (dr == MessageBoxResult.Cancel)
                            {
                                cancelOperation = true;
                            }
                        }));

                        return cancelOperation;
                    }
                }
            }
            catch(Exception ex)
            {
            }
            return false;
        }

        private void branchComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (branchComboBox.Items.Count != 0)
                {
                    populateVersionsComboBox();
                    UpdateCheckBoxesBasedOnBranch();
                }
            }
            catch(Exception ex)
            {
                updateOutputTextBox(new KeyValuePair<Color, string>(Colors.Red, "Unknown Error Occurred Populating Comboboxes: " + ex.ToString() + "\r"));
                AppendTextToLogFile("Exception number for the previous error" + ex.HResult + "\r");
            }
        }

        private void versionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateCheckBoxes(_simTools);
        }

        private void outputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            outputTextBox.ScrollToEnd();
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog browse = new FolderBrowserDialog();
            browse.SelectedPath = sourceDirTextBox.Text;
            browse.ShowDialog();
            sourceDirTextBox.Text = (browse.SelectedPath == "" ? sourceDirTextBox.Text : browse.SelectedPath);
        }

        private void sadRobotCheckBox_ChangedState(object sender, RoutedEventArgs e)
        {
            sadRobotComboBox.IsEnabled = (sadRobotCheckBox.IsChecked == true);
        }

        private void importConfigButton_Click(object sender, RoutedEventArgs e)
        {
            // Select the "import" radio button
            configRadioButton_import.IsChecked = true;

            //// Pop up a folder browser
            //FolderBrowserDialog browse = new FolderBrowserDialog();
            //browse.SelectedPath = configPath;
            //browse.ShowDialog();
            //configPath = (browse.SelectedPath == "" ? configPath : browse.SelectedPath);
            FileDialog browse = new OpenFileDialog();
            browse.Title = "Select Config File";
            browse.Filter = "Config|*.srBackUp;*.zip|All Files|*.*";
            browse.InitialDirectory = configPath;
            browse.ShowDialog();
            configPath = (browse.FileName == "" ? configPath : browse.FileName);

            // Update selected config text
            selectedConfigText.Text = "Selected Config:\n" + configPath;
        }

        private void generateNewConfigButton_Click(object sender, RoutedEventArgs e)
        {
            // Select the "generate" radio button
            configRadioButton_generate.IsChecked = true;

            // Pop up a mini ConfigGen window
            _configGenWindow.ShowDialog();
        }

        private void scheduleButton_Click(object sender, RoutedEventArgs e)
        {
            // Pop up a schedule config window
            _scheduleConfigWindow.ShowDialog();

            // Update the schedule info in the main window when done
            updateScheduleInfo();
        }

        private void optionsScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            bool isVisible = optionsScrollViewer.ComputedVerticalScrollBarVisibility == Visibility.Visible;

            shiftLeft(simToolsLabel, 339, isVisible ? 10 : 0);
            shiftLeft(HHLoggerCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(seaDataLoggerCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(seaDataViewerCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(sadRobotCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(amumuCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(uiAutomationCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(auxGemCheckBox, 339, isVisible ? 10 : 0);
            shiftLeft(downloadOptionsSeparator, 310, isVisible ? 5 : 0);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        private void ConfigRadioButton_existing_Checked(object sender, RoutedEventArgs e)
        {
            // Hide selected config text
            selectedConfigText.Visibility = Visibility.Hidden;
        }

        private void ConfigRadioButton_generate_Checked(object sender, RoutedEventArgs e)
        {
            // Hide selected config text
            selectedConfigText.Visibility = Visibility.Hidden;
        }

        private void ConfigRadioButton_import_Checked(object sender, RoutedEventArgs e)
        {
            // Show selected config text
            selectedConfigText.Visibility = Visibility.Visible;
        }

        #endregion

        private void StopInstallButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void simulatorDirectory_Click(object sender, RoutedEventArgs e)
        {
            //// Pop up a folder browser
            CommonOpenFileDialog browse = new CommonOpenFileDialog();
            browse.IsFolderPicker = true;
            browse.Title = "Select Sim Tools Folder";
            browse.ShowDialog();
            try
            {
                if(_settings != null)
                {
                    selectedSimDirectoryText.Text = (browse.FileName == "" ? _settings.SimDirectory : browse.FileName);
                    _settings.SimDirectory = selectedSimDirectoryText.Text;
                    XMLFunctions.SaveClassToFile(_settings, ".", "settings.xml");
                }
            }
            catch { }
        }

        private void UpdatePoseidon_Click(object sender, RoutedEventArgs e)
        {
            // Set up install script process
            Process updatePoseidon = new Process();
            updatePoseidon.StartInfo.FileName = "powershell.exe";
            updatePoseidon.StartInfo.Arguments = ".\\Update_Poseidon.ps1";
            updatePoseidon.StartInfo.UseShellExecute = false;
            updatePoseidon.StartInfo.CreateNoWindow = true;

            // Launch install script asynchronously and start redirecting output
            updatePoseidon.Start();
        }

        private void UpdatePoseidonSettings_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_settings != null)
                {
                    _settings.IsRTCAnR50 = IsRTCAnR50CheckBox.IsChecked.Value;
                    XMLFunctions.SaveClassToFile(_settings, ".", "settings.xml");
                }
            }
            catch { }
        }

        private void amumuCheckBox_ChangedState(object sender, RoutedEventArgs e)
        {
            amumuComboBox.IsEnabled = (amumuCheckBox.IsChecked == true);
        }

        private void override_Checked(object sender, RoutedEventArgs e)
        {
            if(overrideCheckBox.IsChecked.Value)
            {
                installButton.IsEnabled = true;
                downloadCheckBox.IsEnabled = true;
                installCheckBox.IsEnabled = true;
                sadRobotCheckBox.IsEnabled = true;
                amumuCheckBox.IsEnabled = true;
                HHLoggerCheckBox.IsEnabled = true;
                seaDataLoggerCheckBox.IsEnabled = true;
            }
            else
            {
                installButton.IsEnabled = false;
                downloadCheckBox.IsEnabled = false;
                installCheckBox.IsEnabled = false;
                sadRobotCheckBox.IsEnabled = false;
                amumuCheckBox.IsEnabled = false;
                HHLoggerCheckBox.IsEnabled = false;
                seaDataLoggerCheckBox.IsEnabled = false;
            }
        }

        private void simpleAdvancedBtn_Click(object sender, RoutedEventArgs e)
        {
            if(overrideCheckBox.Visibility == Visibility.Visible)
            {
                simpleAdvancedBtn.Content = "Advanced";
                overrideCheckBox.Visibility = Visibility.Hidden;
                simulatorDirectory.Visibility = Visibility.Hidden;
                selectedSimDirectoryText.Visibility = Visibility.Hidden;
                IsRTCAnR50CheckBox.Visibility = Visibility.Hidden;
            }
            else
            {
                simpleAdvancedBtn.Content = "Simplified";
                overrideCheckBox.Visibility = Visibility.Visible;
                simulatorDirectory.Visibility = Visibility.Visible;
                selectedSimDirectoryText.Visibility = Visibility.Visible;
                IsRTCAnR50CheckBox.Visibility = Visibility.Visible;
            }
        }
    }
}