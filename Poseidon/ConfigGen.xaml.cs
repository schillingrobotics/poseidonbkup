﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Poseidon
{
    /// <summary>
    /// Interaction logic for ConfigGen.xaml
    /// </summary>
    public partial class ConfigGen : Window
    {
        private bool _supportsGemini = true;
        public bool SupportsGemini 
        { 
            get
            {
                return _supportsGemini;
            }
            set
            {
                _supportsGemini = value;

                if (!_supportsGemini)
                {
                    if(vehicleTypeRadioButton_Gemini.IsChecked == true)
                        vehicleTypeRadioButton_UHD3.IsChecked = true;
                }
                else
                {
                    vehicleTypeRadioButton_Gemini.IsEnabled = true;
                }
            }
        }

        public ConfigGen()
        {
            InitializeComponent();
            loadConfig();
        }

        private void loadConfig()
        {
            if (!_supportsGemini)
            {
                vehicleTypeRadioButton_Gemini.IsEnabled = false;
            }

            Config _config = ((MainWindow) Application.Current.MainWindow).Configuration;

            // Load vehicle type
            switch (_config.SysType)
            {
                case Config.SystemType.HD:
                    _config.SysName = Config.SystemName.HD.ToString();
                    vehicleTypeRadioButton_HD.IsChecked = true;
                    break;
                case Config.SystemType.UHDII:
                    _config.SysName = Config.SystemName.UHD2.ToString();
                    vehicleTypeRadioButton_UHD2.IsChecked = true;
                    break;
                case Config.SystemType.UHDIII:
                    _config.SysName = Config.SystemName.UHD3.ToString();
                    vehicleTypeRadioButton_UHD3.IsChecked = true;
                    break;
                default:
                    _config.SysName = Config.SystemName.Gemini.ToString();
                    vehicleTypeRadioButton_Gemini.IsChecked = true;
                    break;
            }

            // Load hardware usage
            leftPodHardwareCheckBox.IsChecked = _config.LeftPodUsesHardware;
            rightPodHardwareCheckBox.IsChecked = _config.RightPodUsesHardware;
            useGameControllerHW.IsChecked = _config.UseGameControllerHardware;

            // Load other parameters
            isol8LicenseCheckBox.IsChecked = _config.ISOL8License;
            useAlternateAuxBox.IsChecked = _config.UseAlternateAux;
        }

        private void vehicleTypeRadioButton_ChangedState(object sender, RoutedEventArgs e)
        {
            if (!_supportsGemini)
            {
                vehicleTypeRadioButton_Gemini.IsEnabled = false;
                if (vehicleTypeRadioButton_Gemini.IsChecked == true)
                {
                    vehicleTypeRadioButton_UHD3.IsChecked = true;
                    return;
                }
            }

            if (vehicleTypeRadioButton_HD.IsChecked == true) 
            { 
                ((MainWindow)Application.Current.MainWindow).Configuration.SysType = Config.SystemType.HD;
                ((MainWindow)Application.Current.MainWindow).Configuration.SysName = Config.SystemName.HD.ToString();
                isol8LicenseCheckBox.IsChecked = false;
                isol8LicenseCheckBox.IsEnabled = false;
            }
            else if (vehicleTypeRadioButton_UHD2.IsChecked == true) 
            { 
                ((MainWindow)Application.Current.MainWindow).Configuration.SysType = Config.SystemType.UHDII;
                ((MainWindow)Application.Current.MainWindow).Configuration.SysName = Config.SystemName.UHD2.ToString();
                isol8LicenseCheckBox.IsChecked = false;
                isol8LicenseCheckBox.IsEnabled = false;
            }
            else if (vehicleTypeRadioButton_UHD3.IsChecked == true) 
            { 
                ((MainWindow)Application.Current.MainWindow).Configuration.SysType = Config.SystemType.UHDIII;
                ((MainWindow)Application.Current.MainWindow).Configuration.SysName = Config.SystemName.UHD3.ToString();
                isol8LicenseCheckBox.IsEnabled = true;
            }
            else if (vehicleTypeRadioButton_Gemini.IsChecked == true) 
            { 
                ((MainWindow)Application.Current.MainWindow).Configuration.SysType = Config.SystemType.Gemini;
                ((MainWindow)Application.Current.MainWindow).Configuration.SysName = Config.SystemName.Gemini.ToString();
                isol8LicenseCheckBox.IsEnabled = true;
            }
        }

        private void leftPodHardwareCheckBox_ChangedState(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Configuration.LeftPodUsesHardware = (leftPodHardwareCheckBox.IsChecked == true);
        }

        private void rightPodHardwareCheckBox_ChangedState(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Configuration.RightPodUsesHardware = (rightPodHardwareCheckBox.IsChecked == true);
        }

        private void useGameControllerHardware_ChangedState(object sender, RoutedEventArgs e)
		{
            ((MainWindow)Application.Current.MainWindow).Configuration.UseGameControllerHardware = (useGameControllerHW.IsChecked == true);
		}

        private void isol8LicenseCheckBox_ChangedState(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Configuration.ISOL8License = (isol8LicenseCheckBox.IsChecked == true);
        }

        private void useAlternateAuxBox_ChangedState(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Configuration.UseAlternateAux = (useAlternateAuxBox.IsChecked == true);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void enableDataSendingCheckbox_ChangedState(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Configuration.EnableDataSending = (enableDataSendingCheckbox.IsChecked == true);
        }

        private void isWindowedModeCheckbox_ChangedState(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Configuration.IsWindowedMode = (isWindowedModeCheckbox.IsChecked == true);
        }
    }
}
