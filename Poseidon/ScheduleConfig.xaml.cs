﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using Application = System.Windows.Application;
using CheckBox = System.Windows.Controls.CheckBox;
using Path = System.IO.Path;

namespace Poseidon
{
    public class Schedule
    {
        public bool IsEnabled;
        public string Branch;
        public List<bool> Days;

        public DateTime NextRun;
        public DateTime PreviousRun;

        public Schedule() { }

        public Schedule(bool initialize)
        {
            if (initialize)
            {
                IsEnabled = false;
                Branch = "";
                Days = new List<bool>();

                Days.Add(true);  // Sunday
                Days.Add(true);  // Monday
                Days.Add(true);  // Tuesday
                Days.Add(true);  // Wednesday
                Days.Add(true);  // Thursday
                Days.Add(false); // Friday
                Days.Add(false); // Saturday

                NextRun = new DateTime();
                PreviousRun = new DateTime();
            }
        }
    }

    /// <summary>
    /// Interaction logic for ScheduleConfig.xaml
    /// </summary>
    public partial class ScheduleConfig : Window
    {
        public Schedule Schedule;

        private List<CheckBox> _dayCheckBoxes = new List<CheckBox>();
        private DateTime _nextRun;

        private bool _donePopulating = false;

        public ScheduleConfig()
        {
            InitializeComponent();
            PopulateUIElements();
            loadSchedule();
            _donePopulating = true;
            saveSchedule();
        }

        public void UpdateNextRun()
        {
            Schedule.PreviousRun = _nextRun;
            Schedule.NextRun = determineNextRun(true);

            XMLFunctions.SaveClassToFile(Schedule, ".", "schedule.xml");
        }

        private void PopulateUIElements()
        {
            // Branches combo box
            //((MainWindow) Application.Current.MainWindow).updateComboBox(branchComboBox, ((MainWindow) Application.Current.MainWindow).branchComboBox.Items.Cast<object>().ToArray());
            ((MainWindow)Application.Current.MainWindow).updateComboBox(branchComboBox, ((MainWindow)Application.Current.MainWindow).branchComboBox.Items.Cast<object>().ToList());
            branchComboBox.DisplayMemberPath = "Key";
            branchComboBox.SelectedValuePath = "Value";

            // Day check boxes
            _dayCheckBoxes.Add(mondayCheckBox);
            _dayCheckBoxes.Add(tuesdayCheckBox);
            _dayCheckBoxes.Add(wednesdayCheckBox);
            _dayCheckBoxes.Add(thursdayCheckBox);
            _dayCheckBoxes.Add(fridayCheckBox);
            _dayCheckBoxes.Add(saturdayCheckBox);
            _dayCheckBoxes.Add(sundayCheckBox);

            // Time combo boxes
            for (int i = 1; i < 13; i++) { hourComboBox.Items.Add(i.ToString()); }
            for (int i = 0; i < 60; i++) { minuteComboBox.Items.Add(i.ToString("00")); }
            ampmComboBox.Items.Add("AM");
            ampmComboBox.Items.Add("PM");
        }

        private void loadSchedule()
        {
            // Parse schedule.xml to load automated schedule
            Schedule = XMLFunctions.PopulateClassFromFile<Schedule>("schedule.xml");

            // If schedule.xml doesn't exist, create a new default schedule object and save it to schedule.xml
            if (Schedule == null)
            {
                Schedule = new Schedule(true);
                XMLFunctions.SaveClassToFile(Schedule, ".", "schedule.xml");
            }

            _nextRun = Schedule.NextRun;

            hourComboBox.SelectedItem = ((Schedule.NextRun.Hour % 12) != 0 ? (Schedule.NextRun.Hour % 12) : 12).ToString();
            minuteComboBox.SelectedItem = Schedule.NextRun.Minute.ToString("00");
            ampmComboBox.SelectedItem = ((int)(Schedule.NextRun.Hour / 12)) == 0 ? "AM" : "PM";

            
            sundayCheckBox.IsChecked = Schedule.Days[0];
            mondayCheckBox.IsChecked = Schedule.Days[1];
            tuesdayCheckBox.IsChecked = Schedule.Days[2];
            wednesdayCheckBox.IsChecked = Schedule.Days[3];
            thursdayCheckBox.IsChecked = Schedule.Days[4];
            fridayCheckBox.IsChecked = Schedule.Days[5];
            saturdayCheckBox.IsChecked = Schedule.Days[6];

            if (Schedule.IsEnabled) { updateDayCheckBoxes(true); }
            else { updateDayCheckBoxes(false); }

            enabledCheckBox.IsChecked = true;
            enabledCheckBox.IsChecked = Schedule.IsEnabled;

            if (Schedule.Branch == "")
            {
                if (branchComboBox.Items.Count != 0) branchComboBox.SelectedIndex = 0;
            }
            else branchComboBox.SelectedValue = Schedule.Branch;
        }

        private void saveSchedule()
        {
            Schedule.IsEnabled = (enabledCheckBox.IsChecked == true);
            Schedule.Branch = branchComboBox.SelectedValue == null ? "" : branchComboBox.SelectedValue.ToString();

            Schedule.Days[0] = (sundayCheckBox.IsChecked == true);
            Schedule.Days[1] = (mondayCheckBox.IsChecked == true);
            Schedule.Days[2] = (tuesdayCheckBox.IsChecked == true);
            Schedule.Days[3] = (wednesdayCheckBox.IsChecked == true);
            Schedule.Days[4] = (thursdayCheckBox.IsChecked == true);
            Schedule.Days[5] = (fridayCheckBox.IsChecked == true);
            Schedule.Days[6] = (saturdayCheckBox.IsChecked == true);

            Schedule.NextRun = _nextRun;

            XMLFunctions.SaveClassToFile(Schedule, ".", "schedule.xml");
        }

        private void updateDayCheckBoxes(bool isEnabled)
        {
            foreach (CheckBox checkBox in _dayCheckBoxes)
            {
                checkBox.IsEnabled = isEnabled;
            }
        }

        private DateTime determineNextRun(bool external = false)
        {
            int setHour, setMinute;

            if (external)
            {
                setHour = Schedule.NextRun.Hour;
                setMinute = Schedule.NextRun.Minute;
            }
            else
            {
                setHour = (int.Parse(hourComboBox.SelectedItem as string) % 12) + ((ampmComboBox.SelectedItem as string) == "PM" ? 12 : 0);
                setMinute = int.Parse(minuteComboBox.SelectedItem as string);
            }
            
            DateTime now = DateTime.Now;
            DateTime nextRun = new DateTime(now.Year, now.Month, now.Day, setHour, setMinute, 0);

            // Compare current time to set time to determine if the next run could be today or not
            if (now > nextRun) nextRun = nextRun.AddDays(1);

            // Starting with the next possible day we could run, compare to the day checkboxes to determine the next actual run day
            int dow = (int)nextRun.DayOfWeek;
            for (int i = dow; i < dow + 7; i++)
            {
                switch (i % 7)
                {
                    case 0:
                        if (external ? Schedule.Days[0]: sundayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                    case 1:
                        if (external ? Schedule.Days[1] : mondayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                    case 2:
                        if (external ? Schedule.Days[2] : tuesdayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                    case 3:
                        if (external ? Schedule.Days[3] : wednesdayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                    case 4:
                        if (external ? Schedule.Days[4] : thursdayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                    case 5:
                        if (external ? Schedule.Days[5] : fridayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                    case 6:
                        if (external ? Schedule.Days[6] : saturdayCheckBox.IsChecked == true) { return nextRun.AddDays(i - dow); }
                        break;
                }

                if (i == dow + 6)
                {
                    return new DateTime(1, 1, 1, setHour, setMinute, 0);
                }
            }

            return new DateTime(1, 1, 1, setHour, setMinute, 0);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)(e.NewValue) == true)
            {
                loadSchedule();
            }
            else
            {
                saveSchedule();
            }
        }

        private void enabledCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            updateDayCheckBoxes(true);

            branchComboBox.IsEnabled = true;
            hourComboBox.IsEnabled = true;
            minuteComboBox.IsEnabled = true;
            ampmComboBox.IsEnabled = true;

            _nextRun = determineNextRun();

            if (_nextRun.Year == 1) nextRunText.Text = "Never";
            else nextRunText.Text = _nextRun.ToString();
        }

        private void enabledCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            updateDayCheckBoxes(false);

            branchComboBox.IsEnabled = false;
            hourComboBox.IsEnabled = false;
            minuteComboBox.IsEnabled = false;
            ampmComboBox.IsEnabled = false;

            _nextRun = new DateTime(1, 1, 1, (int.Parse(hourComboBox.SelectedItem as string) % 12) + ((ampmComboBox.SelectedItem as string) == "PM" ? 12 : 0), int.Parse(minuteComboBox.SelectedItem as string), 0);

            nextRunText.Text = "Disabled";
        }

        private void Time_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_donePopulating)
            {
                _nextRun = determineNextRun();

                if (_nextRun.Year == 1) nextRunText.Text = "Never";
                else nextRunText.Text = _nextRun.ToString();
            }
        }

        private void CheckBoxes_Changed(object sender, RoutedEventArgs e)
        {
            if(_donePopulating)
            {
                _nextRun = determineNextRun();

                if (_nextRun.Year == 1) nextRunText.Text = "Never";
                else nextRunText.Text = _nextRun.ToString();
            }
        }
    }
}
